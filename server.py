"""

    server.py

    This file is dedicated to utilizing our
    network stack and maintaining protocol

    Author: Baldwin Chang <baldwin@baldwinc.com>
    Date: February 13, 2016

"""

from twisted.internet import reactor
from autobahn.twisted.websocket import WebSocketServerFactory
from autobahn.twisted.websocket import WebSocketServerProtocol

"""

    The purpose of using Twisted as our library is
    to not re-invent the wheel. Twisted is a well-known
    and well-tested library for building network-based
    projects.

    It handles the intricacies the of the networking
    stack, and uses some best-use practices.

    We are also using Python due to the fact that
    managing buffers is much easier and less
    error-prone compared to alternatives such as C++.

    We aren't a time-sensitive project, therefore
    the minute time difference in selecting Python
    over C++ is negligible.

"""

"""

    State information:

    S->CONNECTION_RECV:
       Connection has been established

       CONNECTION_AWAIT:
       We are awaiting identification with a client

       CONNECTION_SESSION:
       User has identified and is now able to interact with the
       current session

"""

language = {"en":
                {"client_connect":      "Connection established with a client. Awaiting password.",
                 "client_lost_unid":    "Connection lost with an unidentified client.",
                 "client_lost_id":      "{} lost connection.",
                 "verified_unid":         "Connection has responded with the session password. Awaiting identification.",
                 "invalid_format":      "Client specified an invalid message format.",
                 "invalid_password":    "Client specified invalid password.",
                 "id_connected":        "Client has identified as '{}' and is now a part of session id: {}",
                 "id_connected_no_session":        "Client has identified as '{}', but there are no active sessions.",
                 "disconnected":        "Client has quit the session gracefully.",
                 "id_taken":            "Client has chosen an already taken id. Re-asking."}}

connections = {}
session = None


class SDP(WebSocketServerProtocol):
    global connections
    global session

    sdp_state = "CONNECTION_RECV"
    unique_id = None
    lang = 'en'
    session = session

    def onConnect(self, request):
        self.debug_message(language[self.lang]['client_connect'])
        self.sdp_state = "CONNECTION_AWAIT"
        self.session = session

    def onOpen(self):
        pass

    def onClose(self, wasClean, code, reason):
        if self.unique_id in connections:
            del connections[self.unique_id]
            self.debug_message(language[self.lang]['client_lost_id'].format(self.unique_id))
        else:
            self.debug_message(language[self.lang]['client_lost_unid'])

    def onMessage(self, payload, isBinary):
        message = str(payload.decode('utf8')).strip().split(" ")

        if self.sdp_state == "CONNECTION_AWAIT":
            self.connection_await(message)
        elif self.sdp_state == "CONNECTION_VERIFIED":
            self.connection_verified(message)
        elif self.sdp_state == "CONNECTION_SESSION":
            self.connection_session(message)

    def valid_message_format(self, message, length, error_message):
        if len(message) == length:
            return True
        else:
            # Message length invalid
            self.end_connection(error_message)

    def end_connection(self, debug_message):
        self.debug_message(debug_message)
        self.transport.loseConnection()

    # State Machine States
    def connection_await(self, message):
        if self.valid_message_format(message, 1, language[self.lang]['invalid_format']):
            if message[0] == self.session.password:
                self.sdp_state = "CONNECTION_VERIFIED"
                self.debug_message(language[self.lang]['verified_unid'])
                self.sendLine("IDENTIFY")
            else:
                self.sendLine("INVALID")
                self.end_connection(language[self.lang]['invalid_password'])

    def connection_verified(self, message):
        if self.valid_message_format(message, 1, language[self.lang]['invalid_format']):
            if message[0] in connections.keys():
                self.debug_message(language[self.lang]['id_taken'])
                self.sendLine("ID TAKEN")
            else:
                self.unique_id = message[0]
                connections[self.unique_id] = self
                self.sdp_state = "CONNECTION_SESSION"
                if self.session.in_session():
                    self.sendLine("IDENTIFIED. IN SESSION #{}".format(self.session.session_id()))
                    self.debug_message(language[self.lang]['id_connected'].format(self.unique_id, self.session.session_id()))
                else:
                    self.sendLine("IDENTIFIED. AWAITING SESSION.".format(self.session.session_id()))
                    self.debug_message(language[self.lang]['id_connected_no_session'].format(self.unique_id))

    def connection_session(self, message):
        if message[0] == "QUIT":
            self.sendLine("GOODBYE")
            self.end_connection(language[self.lang]['disconnected'])
        else:
            if self.session.in_session():
                if message[0] == "REPORT" and len(message) == 4:
                    try:
                        for i in range(len(message[1:])):
                            message[i+1] = float(message[i+1])

                        if self.session.add_data_point(self.unique_id, {'x': message[1],
                                                                        'y': message[2],
                                                                        'a': message[3]}):
                            self.debug_message("REPORT: {} @ ({:.2f}, {:.2f}, {:.2f})".format(self.unique_id, float(message[1]), float(message[2]), float(message[3])))
                            self.sendLine("REPORT SUCCESS")
                        else:
                            self.sendLine("REPORT FAILED")
                    except ValueError:
                        self.sendLine("INVALID FORMAT")
                elif message[0] == "LATEST":
                    result = self.session.list_latest_data_points()
                    self.sendLine("{}".format(len(result)))
                    for row in result:
                        self.sendLine("{} {} {} {} {}".format(row['unique_id'], row['x'], row['y'], row['a'], row['time']))
                elif message[0] == "PING":
                    if self.session.in_session():
                        self.sendLine("IN SESSION: {}".format(self.session.session_id()))
                else:
                    self.sendLine("INVALID FORMAT")
            else:
                self.sendLine("NO ACTIVE SESSION")

    def sendLine(self, message):
        self.sendMessage(message)

    @staticmethod
    def debug_message(message):
        line_format = "[ SERVER ] {}".format(message)
        print(line_format)


###
# Functions
###


def send_all(message):
    for user in connections.keys():
        connections[user].sendLine(message)
        SDP.debug_message("sending {}".format(message))


def run(port):
    factory = WebSocketServerFactory()
    factory.protocol = SDP
    reactor.listenTCP(port, factory)
    reactor.run(installSignalHandlers=0)