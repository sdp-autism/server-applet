"""

    session_control.py

    This file contains the representation of our
    session.

    Author: Baldwin Chang <baldwin@baldwinc.com>
    Date: February 13, 2016

"""

import time


class Session:

    def __init__(self, password, sql):
        self.password = password
        self.sql = sql

        self.sid = 0
        self.session_running = False
        self.start_time = 0

    def __del__(self):
        self.end_session()

    def close(self):
        self.__del__()

    def begin_session(self):
        if not self.in_session():
            self.session_running = True
            self.start_time = int(time.time())
            self.sql.query("""INSERT INTO `sdp`.`session_db` (`session_start`) VALUES ('{}');""".format(
                    self.start_time))
            result = self.sql.query("""SELECT `id` FROM `sdp`.`session_db` ORDER by `id` DESC LIMIT 1""")
            self.sid = result[0]['session_db.id']

            self.debug_message("A session has begun successfully with ID: {}".format(self.session_id()))

    def elapsed_time(self):
        if self.in_session():
            return time.time() - self.start_time
        else:
            return 0

    def end_session(self):
        if self.in_session():
            self.session_running = False
            self.sql.query("""UPDATE `sdp`.`session_db` SET `session_end`='{}' WHERE `id`='{}';""".format(
                    int(time.time()), self.sid))
            self.sid = 0
            self.start_time = 0

            self.debug_message("The session has been successfully ended.")

    def in_session(self):
        return self.session_running

    def session_id(self):
        return self.sid

    def get_last_data_point(self, unique_id):
        pass

    def add_data_point(self, unique_id, data):
        if self.in_session():
            self.sql.query("""INSERT INTO `sdp`.`datapoints_db` (`session_id`, `unique_id`, `x_coord`,
            `y_coord`, `alpha`, `timestamp`) VALUES ('{}', '{}', '{}', '{}', '{}', {});""".format(self.session_id(), unique_id, data['x'],
                                                                         data['y'], data['a'], int(time.time())))
            return True
        else:
            return False

    def list_latest_data_points(self):
        if self.in_session():
            result = self.sql.query("""SELECT * FROM `sdp`.`datapoints_db` WHERE `session_id` = '{}' AND `id` IN (SELECT MAX(id)
            FROM `sdp`.`datapoints_db` GROUP BY `unique_id`);""".format(self.session_id()))
            organized = []
            for row in result:
                organized.append({'unique_id': row['datapoints_db.unique_id'], 'x': row['datapoints_db.x_coord'],
                                  'y': row['datapoints_db.y_coord'], 'a': row['datapoints_db.alpha'],
                                  'time': row['datapoints_db.timestamp']})
            return organized
        else:
            return False

    @staticmethod
    def debug_message(message):
        print("[  SESS  ] {}".format(message))


if __name__ == '__main__':
    pass
