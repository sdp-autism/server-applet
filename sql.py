"""

    sql.py

    This file is dedicated to maintaining our
    SQL database queries

    Author: Baldwin Chang <baldwin@baldwinc.com>
    Date: February 13, 2016

"""

import _mysql


class Sql:

    def __init__(self, host, port, user, password, db):
        self._sql = _mysql.connect(host=host, port=port, user=user, passwd=password, db=db)
        self.debug_message("Connected to MySQL database...")

    def __del__(self):
        self._sql.close()
        self.debug_message("Disconnected from MySQL database...")

    def close(self):
        pass

    def query(self, query):
        try:
            self._sql.query(query)
            rows = []
            result = self._sql.store_result()
            if not isinstance(result, type(None)):
                row = result.fetch_row(how=2)
                while len(row) != 0:
                    rows.append(row[0])
                    row = result.fetch_row(how=2)

            return rows
        except _mysql.MySQLError:
            return False

    @staticmethod
    def debug_message(message):
        print("[ MYSQL  ] {}".format(message))

if __name__ == '__main__':
    pass
