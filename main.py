"""

    main.py

    This file is the entry point for our
    servlet.

    Author: Baldwin Chang <baldwin@baldwinc.com>
    Date: February 13, 2016

"""

import settings
import server
import sql
import session_control
import threading
import time


def server_daemon(port):
    def process():
        server.run(port)

    return process


def debug_message(message):
    print("[ DEBUG  ] {}".format(message))

if __name__ == '__main__':
    db = sql.Sql(settings.mysql_host, settings.mysql_port, settings.mysql_username, settings.mysql_password,
                 settings.mysql_database)
    session = session_control.Session(settings.server_password, db)

    server.session = session

    debug_message("Starting server on port {}...".format(settings.server_port))
    server_thread = threading.Thread(target=server_daemon(settings.server_port))
    server_thread.daemon = True
    server_thread.start()
    debug_message("Server successfully started! Type 'help' for more information.")

    while True:
        cli = raw_input().lower()
        if cli == "quit":
            # Close things gracefully
            debug_message("Closing all connections...")
            for user in server.connections.keys():
                server.connections[user].sendLine("Your session has ended.")
                server.connections[user].end_connection("CLOSING CONNECTION")
            debug_message("All connections closed.")

            session.end_session()
            db.close()
            break
        elif cli == "session begin":
            if not session.in_session():
                session.begin_session()
                server.send_all("SESSION BEGIN: {}".format(session.session_id()))
            else:
                debug_message("A session is currently running. (ID: {})".format(session.session_id()))
        elif cli == "session end":
            if session.in_session():
                session.end_session()
                server.send_all("SESSION END")
            else:
                debug_message("There is no session to end.")
        elif cli == "session status":
            if session.in_session():
                debug_message("Session active. ID: {}".format(session.session_id()))
                debug_message("Session length: {}m".format(session.elapsed_time() / 60))
                debug_message("There {} currently {} {}.".format("are" if len(server.connections) != 1 else "is",
                                                                 len(server.connections),
                                                                 "connections" if len(server.connections) != 1
                                                                 else "connection"))

                # Only print out extra data if we have data to associate with it
                if len(server.connections):
                    for user in server.connections.keys():
                        debug_message("User '{}'".format(user))

                    latest = session.list_latest_data_points()
                    for result in latest:
                        debug_message("{} last @ ({}, {}, {}) {}s ago".format(result['unique_id'], result['x'], result['y'],
                                                                      result['a'], int(time.time()-int(result['time']))))
            else:
                debug_message("No session currently active.")
        else:
            print("""Help menu:
            session begin - begin a new session
            session end - terminate the current session

            session status - list some statistics from the session
            """)
